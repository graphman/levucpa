root = this

DownloadCSV : (filename, data) ->
  content = "data:text/csv;charset=utf-8,#{data.join("\n")}"
  dl = $ "<a/>",
    download: "#{filename}.csv"
    href: window.encodeURI content

  (dl.get 0).click()

root.Database = class Database
  @enabled : false

  @open : ->
    Database.db = window.openDatabase "Ucpaleveler",
      "1.0", "Ucpaleveler storage", 5 * 1024 * 1024

  @reset : ->
    Database.db.transaction (tx) ->
      tx.executeSql "DELETE FROM results", [],
        Database.onSuccess, Database.onError

  @isEnabled : ->
    if window.openDatabase?
      Database.enabled = true
      window.localStorage.getItem "db" is "on" and Database.enabled

  @disable : ->
    window.localStorage.setItem "db", "off"

  @enable : ->
    window.localStorage.setItem "db", "off"

  @onError : (tx, error)  ->
    console.log "Error: #{error.message}"

  @onSuccess : ->

  @init : ->
    Database.db.transaction (tx) ->
      tx.executeSql """
        CREATE TABLE IF NOT EXISTS results("
        first_name VARCHAR(255) NOT NULL,
        last_name VARCHAR(255) NOT NULL,
        age INTEGER NOT NULL,
        stage_id INTEGER NOT NULL,
        level integer NOT NULL)
        """, []

  @saveProfile : (profile) ->
    Database.db.transaction (tx) ->
      tx.executeSql """
        INSERT INTO results(first_name, last_name, age, stage_id, level)
        VALUES (?, ?, ?, ?, ?)
        """, [profile.first_name, profile.last_name, profile.age,
              profile.stage_id, profile.level],
              Database.onSuccess, Database.onError

  @saveToCsv : (filename) ->
    results = [["Prénom", "Nom", "Age", "Inscrit dans", "Niveau", "Resultat"]]

    save = (tx, rs) ->
      last_stage = -1
      for i in [0..rs.rows.length]
        profile = rs.rows.item(i)

        if profile.stage_id isnt last_stage
          result.push []
          last_stage = profile.stage_id

        result.push [
          profile.first_name,
          profile.last_name,
          profile.age,
          "#{root.stages[profile.stage_id].title} \
            (Niveau #{root.stages[profile.stage_id].level})",
          profile.level,
          if profile.level >= root.stages[profile.stage_id].level then "OK"
          else "INSUFFISANT"
        ]

        DownloadCSV "resultat-#{(new Date).toLocaleDateString()}", results

    Database.db.transaction (tx) ->
      tx.executeSql "SELECT * FROM results ORDER BY stage_id ASC", [],
        save, Database.onError

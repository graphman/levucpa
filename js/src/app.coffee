root = this

class Profile
  constructor : (@first_name, @last_name, @age, @stage_id) ->
    @level = 0


class UcpaLeveler
  constructor : ->
    @profile = null
    @question_id = -1
    @responses = []
    @result = []
    @question_elements = []
    @current_page = -1

  loadStages : ->
    for stage, index in root.stages
      stage_elem = $ "<option/>",
        value: index
        text: stage.title

      $("#input-stage").append stage_elem

  loadQuestions : ->
    self = @

    createResponseElement = (question, response) ->
      response_element = $ "<div/>",
        class: "ucpa-response col-sm-12"

      response_btn = $ "<button/>",
        text: response.text
        type: "button"
        class: "btn btn-default btn-lg btn-block resp-btn"
        resp_id: response.id
        quest_id: question.id
        on:
          click: ->
            id = $(@).attr "quest_id"
            $(self.responses[id]).removeClass "btn-success"
            $(@).addClass "btn-success"
            self.responses[id] = $(@)
            self.result[id] = $(@).attr "resp_id"

      response_element.append response_btn
      response_element

    createPaginationElement = (question) ->
      pagination = $ "<div/>",
        class: "pagination"

      prev_btn = $("<button/>",
        class: "prev-btn btn btn-default btn-danger"
        quest_id: question.id
        text: if root.is_english then "Previous question" else "Question précédente"
        on:
          click: ->
            self.showPreviousPage()
            $(@).attr "disabled", "disabled"
            setTimeout (=> $(@).removeAttr "disabled"), 500
      ).prepend $("<a/>",
        style: "margin:right: 10px;",
        class: "glyphicon glyphicon-arrow-left"
      )

      next_btn = $("<button/>",
        class: "prev-btn btn btn-default btn-info"
        quest_id: question.id
        text: if root.is_english then "Next question" else "Question suivante"
        on:
          click: ->
            self.showNextPage()
            $(@).attr "disabled", "disabled"
            setTimeout (=> $(@).removeAttr "disabled"), 500
      ).append $("<a/>",
        style: "margin:right: 10px;",
        class: "glyphicon glyphicon-arrow-right"
      )

      if question.id is 0
        pagination.append next_btn
      else
        pagination.append prev_btn
        pagination.append next_btn

      pagination

    createQuestionElement = (question) ->
      question_elem = $ "<div/>",
        id: "ucpa-question-#{question.id}"
        class: "ucpa-page ucpa-question"

      title = $ "<div/>",
        text: question.title
        class: "title"

      question_elem.append title

      responses_elem = $ "<div/>",
        class: "ucpa-responses"

      for element in question.responses
        responses_elem.append createResponseElement question, element

      question_elem.append responses_elem
      question_elem.append createPaginationElement question

      self.question_elements[question.id] = question_elem
      question_elem

    for question in root.questions
      $("#ucpa-questions").append createQuestionElement question

  showResult : ->
    best_rank = -1
    best_level = null
    valid_levels = []
    for level in root.levels
      valid = true
      for requirement in level.requirements
        if !@result[requirement.question] or @result[requirement.question] < requirement.points
          valid = false
      if valid
        if level.rank > best_rank
          best_rank = level.rank
          best_level = level

    @profile.level = best_level.rank


    $("#level").text @profile.level
    $(".stage-level").text root.stages[@profile.stage_id].level

    if root.stages[@profile.stage_id].level > @profile.level
      $("#ucpa-level").css "color", "#d9534f"
      $("#comment-bad").fadeIn "fast"
    else
      $("#ucpa-level").css "color", "#a6d490"
      $("#comment-good").fadeIn "fast"

    if root.Database.isEnabled()
      root.Database.saveProfile @profile

  showNextPage : ->
    toHide = null
    toShow = null

    if @current_page is -1
      toHide = $ "#ucpa-welcome"
      toShow = $ "#ucpa-login"
    else if @current_page is 0
      toHide = $ "#ucpa-login"
      toShow = @question_elements[0]
    else if @current_page is root.questions.length
      toHide = $ @question_elements[root.questions.length - 1]
      toShow = $ "#ucpa-results"
      @showResult()
    else
      toHide = $ @question_elements[@current_page - 1]
      toShow = $ @question_elements[@current_page]

    $(toHide).fadeOut "fast", =>
      $(toShow).fadeIn "fast"
      @current_page++

  showPreviousPage : ->
    toHide = null
    toShow = null

    if 1 < @current_page <= root.questions.length + 1
      toHide = $ @question_elements[@current_page - 1]
      toShow = $ @question_elements[@current_page - 2]

      $(toHide).fadeOut "fast", =>
        $(toShow).fadeIn "fast"
        @current_page--

$ ->
  if root.Database.isEnabled()
    root.Database.open()
    root.Database.init()
    $("#reset-btn").removeAttr "disabled"
    $("#dl-csv-btn").removeAttr "disabled"

  leveler = new UcpaLeveler
  leveler.loadStages()
  leveler.loadQuestions()

  $("#reset-btn").on "click", ->
    if confirm "Etes vous sûr de vouloir supprimer les resultats ?"
      root.Database.reset()

  $("#dl-csv-btn").on "click", ->
    root.Database.saveToCsv "resultats-#{(new Date).toLocaleDateString()}"

  $("#db-enable").bootstrapSwitch
    disabled: !root.Database.enabled
    state: root.Database.isEnabled()
    onSwitchChange: (event, state) ->
      if state
        root.Database.enable()
        root.Database.open()
        root.Database.init()
        $("#reset-btn").removeAttr "disabled"
        $("#dl-csv-btn").removeAttr "disabled"
      else
        root.Database.disable()
        $("#reset-btn").attr "disabled", "disabled"
        $("#dl-csv-btn").attr "disabled", "disabled"
    onText: "ON"
    offText: "OFF"

  $("#restart-btn").on "click", ->
    location.reload()

  $("#reload-btn").on "click", ->
    location.reload()

  $("#start-btn").on "click", ->
    leveler.showNextPage()

  $("#login-form").on "submit", (event) ->
    event.preventDefault()
    last_name = $("#input-lastname").val()
    first_name = $("#input-firstname").val()
    stage_id = $("#input-stage").val()
    age = $("#input-age").val()

    if last_name.length <= 0 or first_name.length <= 0 or
        stage_id == -1 or age.length <= 0
      $("#login-error").css "visibility", "visible"
    else
      $("#login-btn").attr "disabled", "disabled"
      leveler.profile = new Profile first_name, last_name, age, stage_id
      $("#profile-info").fadeIn()

      if first_name.length + last_name.length > 20
        $("#name").text "#{first_name[0]}. #{last_name}"
      else
        $("#name").text "#{first_name} #{last_name}"

      leveler.showNextPage()

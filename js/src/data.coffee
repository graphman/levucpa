root = this

class Stage
  constructor : (@title, @level) ->

class Question
  constructor : (@id, @title, @responses) ->

class Response
  constructor : (@id, @text, @points) ->

class Requirement
  constructor : (@question, @points) ->

class Level
  constructor : (@rank, @requirements) ->

root.stages = [
  (new Stage "Préparation Mont-Blanc", 1),
  (new Stage "Break Initiation expé et trek en altitude", 1),
  (new Stage "Sommet du Bishorn 4159 m", 1),
  (new Stage "Grand Paradis premier 4000", 1),
  (new Stage "Mont-Rose 4563 m", 1),
  (new Stage "Alpinisme spécial débutant", 1),
  (new Stage "Break Altitude", 1),

  (new Stage "Alpinisme 14 jours", 2),
  (new Stage "La Haute Route \"Chamonix Zermatt\"", 2),
  (new Stage "Sommet du Weissmies 4023 m", 2),
  (new Stage "Les Dômes de Miage 3669 m", 2),
  (new Stage "Castor et Lyskamm 4479 m", 2),
  (new Stage "Sommet du Mont-Blanc 4810 m", 2),

  (new Stage "Dom des Mischabel 4545 m", 3),

  (new Stage "Break Alpinisme", 2),
  (new Stage "Alpinisme", 2)
]

if root.is_english
  root.questions = [
    (new Question 0, "Physical Activity", [
      (new Response 0, "I do not practice any sport or physical activity.", 0),
      (new Response 1, "I practive a sport in an irregular manner.", 1),
      (new Response 2, "I run or hike regulary in plains", 2),
      (new Response 3, "I run 10km per week or practice mountain activities \
                    several times a year.", 3)
    ]),
    (new Question 1, "Snow and Ice", [
      (new Response 0, "I have never walked on ice.", 0),
      (new Response 1, "I have already walked o a glacier with ice picks and \
                    crampons.", 1),
      (new Response 2, "I have already attained a snowy summit hight than 3000m.", 2),
      (new Response 3, "I have already attained a snow corridor or done ice climbing.", 3)
    ]),
    (new Question 2, "Indoor climbing", [
      (new Response 0, "I have never climbed indoors.", 0),
      (new Response 1, "I regularly pass a 4a level.", 1),
      (new Response 2, "I regularly pass a 5c/6a level.", 2),
      (new Response 3, "I reuglarly pass a 6a/6b level.", 3)
    ]),
    (new Question 3, "Alpine climbing", [
      (new Response 0, "I have never done any alpine climbing.", 0),
      (new Response 1, "I have already climbed a \"little difficult level\" (III)", 1),
      (new Response 2, "I have already climbed a \"fairly hard level\" (IV).", 2),
      (new Response 3, "I have already climbed a \"difficult level\" (IV+/V).", 3),
      (new Response 4, "I have already climbed a \"very difficult level\" (VI+/VIa).", 4)
    ]),
    (new Question 4, "Technical level", [
      (new Response 0, "I do not know how to rope up.", 0),
      (new Response 1, "I know how to rope up, belay, and rappel.", 1),
      (new Response 2, "I know how to organise a belay station.", 2)
    ]),
    (new Question 5, "A bit more", [
      (new Response 0, "I do not know how to lead.", 0),
      (new Response 1, "I know how to lead.", 1)
    ])
  ]

else
  root.questions = [
    (new Question 0, "Activité physique", [
      (new Response 0, "Je ne fais aucun sport ou activité physique.", 0),
      (new Response 1, "Je fais du sport 1 fois par semaine | Je cours 10km en \
                    moins de 1h15 | Je peux marcher 5h à bon rythme.", 1),
      (new Response 2, "Je fais du sport 2fois par semaine | Je cours 2h sans\
                    m’arrêter | Je peux marcher 7h en montagne à rythme soutenu.", 2),
      (new Response 3, "Je m’entraîne à l’endurance 3fois par semaine | Je cours \
                    3h sans m’arrêter | Je peux marcher 10h en montagne à rythme soutenu.", 3)
    ]),
    (new Question 1, "Neige et glace", [
      (new Response 0, "Je n'ai jamais marché sur de la glace.", 0),
      (new Response 1, "J'ai déjà marché  avec piolets et crampons \
                    sur un glacier.", 1),
      (new Response 2, "J'ai déjà fait un sommet en neige de \
                    plus de 3000m.", 2),
      (new Response 3, "J'ai déjà fait un couloir de neige ou \
                    cascade de glace.", 3)
    ]),
    (new Question 2, "Escalade en salle", [
      (new Response 0, "Je n'ai jamais fais d'escalade en salle.", 0),
      (new Response 1, "Je passe régulièrement du 4a.", 1),
      (new Response 2, "Je passe régulièrement du 5c/6a.", 2),
      (new Response 3, "Je passe régulièrement du 6a/6b.", 3)
    ]),
    (new Question 3, "Escalade en montagne", [
      (new Response 0, "Je n'ai jamais fais d'escalade en montagne.", 0),
      (new Response 1, "J'ai déjà fait du peu difficile  III).", 1),
      (new Response 2, "J'ai déjà fait du assez difficile  IV).", 2),
      (new Response 3, "J'ai déjà fait du difficile  IV+/V).", 3),
      (new Response 4, "J'ai déjà fait du très difficile  VI+/VIa).", 4)
    ]),
    (new Question 4, "Niveau technique", [
      (new Response 0, "Je ne sais pas m'encorder.", 0),
      (new Response 1, "Je sais m'encorder, assurer et faire du rappel.", 1),
      (new Response 2, "Je sais organiser un relais.", 2)
    ]),
    (new Question 5, "En plus", [
      (new Response 0, "Je ne sais pas grimper en tête.", 0),
      (new Response 1, "Je sais grimper en tête.", 1)
    ])
  ]

root.levels = [
  (new Level 1, [
    (new Requirement 0, 1)
  ]),
  (new Level 2, [
    (new Requirement 0, 1),
    (new Requirement 1, 2),
    (new Requirement 2, 1),
    (new Requirement 3, 1),
    (new Requirement 4, 1)
  ]),
  (new Level 3, [
    (new Requirement 0, 2),
    (new Requirement 1, 2),
    (new Requirement 2, 2),
    (new Requirement 3, 2),
    (new Requirement 4, 2)
    (new Requirement 5, 1)
  ]),
  (new Level 4, [
    (new Requirement 0, 3),
    (new Requirement 1, 3),
    (new Requirement 2, 3),
    (new Requirement 3, 4),
    (new Requirement 4, 2),
    (new Requirement 5, 1)
  ])
]
